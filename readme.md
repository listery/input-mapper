# listery/input-mapping

## Installation
Access listery private composer registry as normal.

`composer require listery/input-mapping`

### Laravel Setup
Add `Listery\InputMapping\InputMapperServiceProvider::class` to the 'providers' section of `config/app.php`.

`php artisan vendor:publish --tag=input_mapping`


## Laravel Usage

Add 'mappers' to the `config/input-mapping.php` file.
Each mapper should implement `Listery\InputMapping\Mapping\Mapper` or alternatively extend `ObjectMapper` in the same namespace.

The implmented `map(array $input)` function will receive array input, which can then be
converted to the object of your choice. You may nest objects (for example, map `$input['tags]` from inside the mapper of a class to Tag objects)
by using `$this->mapper->map($input['tags'])->toMany(Tag::class)` inside the map method.
