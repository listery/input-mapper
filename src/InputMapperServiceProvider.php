<?php


namespace Listery\InputMapper;


use Listery\InputMapper\Mapping\Mapper;
use Listery\InputMapper\Mapping\Registry;
use Illuminate\Support\ServiceProvider;

class InputMapperServiceProvider extends ServiceProvider
{
    use Config;

    public function boot()
    {
        $this->publishes(
            [$this->getConfigPath() => config_path($this->getConfigFilename())],
            'input_mapping'
        );
    }

    public function register()
    {
        $this->mergeConfigFrom($this->getConfigPath(), $this->getConfigName());

        $this->app->singleton(Mapper::class, \Listery\InputMapper\Mapper::class);
        $this->app->resolving(Registry::class, function(Registry $registry){
            foreach($this->app->make('config')->get(
                $this->getConfigName() . '.mappers', []
            ) as $class => $mapper)
            {
                $registry->bind($class, $this->getResolverFor($mapper));
            }
        });
    }

    protected function getResolverFor($mappingClass)
    {
        return function() use($mappingClass) {
            return $this->app->make($mappingClass);
        };
    }
}