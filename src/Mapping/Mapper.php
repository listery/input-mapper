<?php


namespace Listery\InputMapper\Mapping;


interface Mapper
{
    public function map($input);
}