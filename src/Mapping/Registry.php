<?php


namespace Listery\InputMapper\Mapping;


use Closure;
use Listery\InputMapper\Exceptions\UndefinedMapperException;
use Listery\InputMapper\Mapper;

class Registry
{
    /**
     * @var Closure[]
     */
    protected $resolvers = [];

    /**
     * @var array | Mapper
     */
    protected $mappers = [];

    /**
     * @var Mapper
     */
    protected $mapper;

    public function bind($class, $resolver)
    {
        $this->resolvers[$class] = $resolver;

        return $this;
    }

    /**
     * @param $class
     * @return Mapper | null
     */
    public function get($class)
    {
        return $this->mappers[$class] ?? $this->resolve($class) ?? null;
    }

    public function setMapper(Mapper $mapper)
    {
        $this->mapper = $mapper;

        return $this;
    }

    /**
     * @param $class
     * @return mixed
     * @throws UndefinedMapperException
     */
    protected function resolve($class)
    {
        $resolver = $this->resolvers[$class] ?? null;

        if (!$resolver) {
            throw new UndefinedMapperException($class);
        }

        $mapper = $resolver($this->mapper);
        return $this->mappers[$class] = $mapper;
    }
}