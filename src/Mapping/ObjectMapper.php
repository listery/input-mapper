<?php


namespace Listery\InputMapper\Mapping;


use Listery\InputMapper\Mapper;

abstract class ObjectMapper
{
    /**
     * @var Mapper
     */
    protected $mapper;

    /**
     * ObjectMapper constructor.
     * @param Mapper $mapper
     */
    public function __construct(Mapper $mapper)
    {
        $this->mapper = $mapper;
    }

    abstract public function map(array $input);
}