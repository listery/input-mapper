<?php


namespace Listery\InputMapper;


use Listery\InputMapper\Mapping\Registry;

class Mapper implements \Listery\InputMapper\Mapping\Mapper
{
    /**
     * @var Registry
     */
    protected $registry;

    public function __construct(Registry $registry)
    {
        $this->registry = $registry;
        $this->registry->setMapper($this);
    }

    public function registry()
    {
        return $this->registry;
    }

    public function map($input)
    {
        return new OutputMapper($this->registry, $this, $input);
    }
}