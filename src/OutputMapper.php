<?php


namespace Listery\InputMapper;


use Listery\InputMapper\Mapping\Registry;

class OutputMapper
{
    /**
     * @var array
     */
    protected $input;

    /**
     * @var Mapper
     */
    protected $mapper;

    public function __construct(Registry $registry, Mapper $mapper, array $input)
    {
        $this->registry = $registry;
        $this->mapper = $mapper;
        $this->input = $input;
    }

    public function to($class)
    {
        return $this->registry->get($class)
            ->map($this->input);
    }

    public function toMany($class)
    {
        $output = [];
        foreach ($this->input as $key => $item) {
            // If not array, try to map the current array to many objects. Tags are a great example of this.
            if(!is_array($item))
            {
                $item = [$key => $item];
            }

            $output[] = $this->registry->get($class)->map($item);
        }

        return $output;
    }
}