<?php


namespace Listery\InputMapper;


trait Config
{
    public function getConfigName() {
        return 'input-mapping';
    }

    public function getConfigPath() {
        return __DIR__ . '/../config/' . $this->getConfigFilename();
    }

    public function getConfigFilename() {
        return $this->getConfigName() . '.php';
    }
}