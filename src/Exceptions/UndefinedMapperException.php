<?php


namespace Listery\InputMapper\Exceptions;


use Exception;

class UndefinedMapperException extends Exception
{
    public function __construct($class)
    {
        parent::__construct("The class '{$class}' has no registered mapper and cannot be resolved.");
    }
}