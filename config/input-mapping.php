<?php

return [
    /*
     |------------------------------------------------------------
     | Mappers
     |------------------------------------------------------------
     |
     | Configure your input mappers here using a key -> value pair.
     | Each mapper should implement the
     | Listery\InputMapper\Mapping\Mapper interface.
     |
     | Then simply inject the Listery\InputMapper\Mapper service
     | and start using it in your controllers, or anywhere else!
     |
     */
    'mappers' => [
        // ObjectClass => ObjectMapper
    ]
];