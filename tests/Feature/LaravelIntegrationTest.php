<?php


namespace Listery\InputMapper\Tests\Feature;


use Listery\InputMapper\Config;
use Listery\InputMapper\InputMapperServiceProvider;
use Listery\InputMapper\Mapper;
use Listery\InputMapper\Mapping\ObjectMapper;
use Listery\InputMapper\Tests\Models\Mappers\UserMapper;
use Listery\InputMapper\Tests\Models\User;
use Illuminate\Contracts\Mail\Mailer;
use Tests\TestCase as LaravelTestCase;

class LaravelIntegrationTest extends LaravelTestCase
{
    use Config;

    protected function setUp()
    {
        parent::setUp();
        $this->app->register(InputMapperServiceProvider::class);
    }

    /**
     * @test
     */
    public function resolves_mappers_from_config()
    {
        $this->app->make('config')->set($this->getConfigName().'.mappers', [User::class => UserMapper::class]);

        /** @var Mapper $mapper */
        $mapper = $this->app->make(Mapper::class);
        $input = ['name' => 'John Doe', 'email' => 'j@ex.com'];

        $user = $mapper->map($input)->to(User::class);
        $this->assertEquals('John Doe', $user->getName());
    }

    /**
     * @test
     */
    public function resolves_mappers_out_of_container()
    {
        $this->app->make('config')->set($this->getConfigName().'.mappers', [User::class => MailedUserMapper::class]);

        /** @var Mapper $mapper */
        $mapper = $this->app->make(Mapper::class);
        $mapping = $mapper->registry()->get(User::class);

        $this->assertInstanceOf(Mailer::class, $mapping->getMailer());
    }

    /**
     * @test
     */
    public function resolves_mapper_using_interface()
    {
        $mapper = $this->app->make(\Listery\InputMapper\Mapping\Mapper::class);
        $this->assertInstanceOf(Mapper::class, $mapper);
    }
}

class MailedUserMapper extends ObjectMapper {

    public function __construct(Mapper $mapper, Mailer $mailer)
    {
        parent::__construct($mapper);
        $this->mailer = $mailer;
    }

    public function getMailer()
    {
        return $this->mailer;
    }

    public function map(array $input)
    {
        return new User();
    }

}