<?php


namespace Listery\InputMapper\Tests\Feature;


use Listery\InputMapper\Mapper;
use Listery\InputMapper\Mapping\Registry;
use Listery\InputMapper\OutputMapper;
use Listery\InputMapper\Tests\Models\Mappers\MessageMapper;
use Listery\InputMapper\Tests\Models\Mappers\TagMapper;
use Listery\InputMapper\Tests\Models\Mappers\UserMapper;
use Listery\InputMapper\Tests\Models\Message;
use Listery\InputMapper\Tests\Models\Tag;
use Listery\InputMapper\Tests\Models\User;
use Listery\InputMapper\Tests\TestCase;

class TransformInputToObjectsTest extends TestCase
{
    /**
     * @var Mapper
     */
    protected $mapper;

    /**
     * @var Registry
     */
    protected $registry;

    protected function setUp()
    {
        parent::setUp();

        $this->registry = new Registry();

        $this->registry->bind(User::class, function (Mapper $mapper) {
            return new UserMapper($mapper);
        });

        $this->registry->bind(Tag::class, function (Mapper $mapper) {
            return new TagMapper($mapper);
        });

        $this->mapper = new Mapper($this->registry);
    }

    /**
     * @test
     */
    public function can_transform_array_input_to_object()
    {
        $input = ['name' => 'John', 'email' => 'johndoe@example.com'];
        $mapper = $this->mapper->map($input);

        /** @var User $output */
        $output = $mapper->to(User::class);

        $this->assertInstanceOf(OutputMapper::class, $mapper);
        $this->assertEquals('John', $output->getName());
        $this->assertEquals('johndoe@example.com', $output->getEmail());
    }

    /**
     * @test
     */
    public function can_transform_array_input_to_many_objects()
    {
        $input = [
            ['name' => 'John', 'email' => 'johndoe@example.com'],
            ['name' => 'Jane', 'email' => 'janedoe@example.com']
        ];

        $output = $this->mapper->map($input)->toMany(User::class);

        $this->assertCount(2, $output);
        $this->assertEquals('Jane', $output[1]->getName());
    }

    /**
     * @test
     */
    public function can_transform_non_nested_array_into_many_objects()
    {
        $input = [
            'test' => 'hello',
            'name' => 'john'
        ];

        $output = $this->mapper->map($input)->toMany(Tag::class);

        $this->assertCount(2, $output);
        $this->assertEquals('john', $output[1]->getValue());
    }

    /**
     * @test
     */
    public function can_transform_related_objects()
    {
        $this->registry->bind(Message::class, function (Mapper $mapper) {
            return new MessageMapper($mapper);
        });
        $this->registry->bind(Tag::class, function (Mapper $mapper) {
            return new TagMapper($mapper);
        });

        $input = [
            ['body' => 'Hello World', 'tags' => [['brand' => 'Acme', 'type' => 'notification']]],
            ['body' => 'Second Message', 'tags' => [['brand' => 'Sweepers Ltd', 'type' => 'notification']]]
        ];

        $output = $this->mapper->map($input)->toMany(Message::class);

        $this->assertCount(2, $output);
        $this->assertInstanceOf(Message::class, $output[0]);
        $this->assertCount(1, $output[0]->getTags());
        $this->assertCount(1, $output[1]->getTags());
        $this->assertEquals('Acme', $output[0]->getTags()[0]->getValue());
    }
}