<?php


namespace Listery\InputMapper\Tests\Models\Mappers;


use Listery\InputMapper\Mapping\ObjectMapper;
use Listery\InputMapper\Tests\Models\Message;
use Listery\InputMapper\Tests\Models\Tag;

class MessageMapper extends ObjectMapper
{
    public function map(array $input)
    {
        return (new Message())->setBody($input['body'])
            ->setTags($this->mapper->map($input['tags'])->toMany(Tag::class) ?? []);
    }

}