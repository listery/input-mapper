<?php


namespace Listery\InputMapper\Tests\Models\Mappers;


use Listery\InputMapper\Mapping\ObjectMapper;
use Listery\InputMapper\Tests\Models\User;

class UserMapper extends ObjectMapper
{
    public function map(array $input)
    {
        return (new User())
            ->setName($input['name'] ?? null)
            ->setEmail($input['email'] ?? null);
    }
}