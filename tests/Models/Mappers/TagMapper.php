<?php


namespace Listery\InputMapper\Tests\Models\Mappers;


use Listery\InputMapper\Mapping\ObjectMapper;
use Listery\InputMapper\Tests\Models\Tag;

class TagMapper extends ObjectMapper
{
    public function map(array $input)
    {
        foreach($input as $key => $value)
        {
            return (new Tag)
                ->setKey($key)
                ->setValue($value);
        }

        return null;
    }
}