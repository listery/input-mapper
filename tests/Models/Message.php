<?php


namespace Listery\InputMapper\Tests\Models;


class Message
{
    /**
     * @var string
     */
    protected $body;

    /**
     * @var Tag[]
     */
    protected $tags = [];

    /**
     * @return string
     */
    public function getBody(): string
    {
        return $this->body;
    }

    /**
     * @param string $body
     * @return Message
     */
    public function setBody(string $body): Message
    {
        $this->body = $body;
        return $this;
    }

    /**
     * @return Tag[]
     */
    public function getTags(): array
    {
        return $this->tags;
    }

    /**
     * @param Tag[] $tags
     * @return Message
     */
    public function setTags(array $tags): Message
    {
        $this->tags = $tags;
        return $this;
    }
}